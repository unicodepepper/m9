import subprocess


def fortune(tokens):
    if len(tokens) > 1:
        raise AssertionError("error: too many args. use /fortune [fortune file]")

    cmd = ["fortune", "-c"]
    if len(tokens) == 1:
        cmd.append(tokens[0])
    res = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output = res.stdout if res.returncode == 0 else res.stderr
    return str(output, encoding="utf-8")
