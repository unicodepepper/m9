# -*- coding: utf-8 -*-

import random


def curry(v):
    return lambda x: v


def alexa(msg):
    idx = msg.find("alexa ")
    q = msg[idx + len("alexa ") :]
    # drop verb
    q = q[q.find(" ") + 1 :]
    import urllib.parse

    q = urllib.parse.quote_plus(q)
    return f"here are some results for '{q}' on the web: https://duckduckgo.com/?q=" + q


handlers = {
    "cum": lambda x: "ok co" + ("o" * random.randint(1, 32)) + "mer",
    "epic": curry("ok this is epic"),
    "cring": curry("message saved to collection 'cringe'"),
    "hentai": curry("reported"),
    "alexa ": alexa,
    "anime": curry("actually, it's pronounced /ˈænəˌmeɪ/"),
}


def annoyance(msg):
    if random.randint(1, 100) < 70:
        return None

    for trigger in handlers:
        if msg.lower().find(trigger) != -1:
            return handlers[trigger](msg)
