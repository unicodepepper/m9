import places_db
import requests
import logging
from datetime import datetime
import pytz

URL = "https://api.openweathermap.org/data/2.5/weather"
API_KEY = "e5a5bd0f8a2108f9de16176c6fab7f64"

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# https://openweathermap.org/weather-conditions
def get_icon(code, local_time_hour):
    if code - 200 < 100:
        # thunder
        return "⛈️"
    elif code - 300 < 100:
        # drizzle
        return "🌦️"
    elif code - 500 < 100:
        # rain
        return "🌧️"
    elif code - 600 < 100:
        # snow
        return "❄️"
    elif code - 600 < 100:
        # atmosphere
        return "🌬️"
    elif code == 800:
        # clear
        if local_time_hour > 6 and local_time_hour < 20:
            return "☀️"
        else:
            return "🌙"  # moon
    elif code - 800 < 100:
        # cloudy
        return "⛅"


def weather(tokens):
    assert len(tokens) > 0, "usage: /weather location"

    query = " ".join(tokens)
    place = places_db.find_place(query)
    assert place != None, f"couldn't find {query}"

    payload = {"q": place.name, "appid": API_KEY, "units": "metric"}

    r = requests.get(URL, params=payload)

    try:
        r.raise_for_status()
    except Exception as e:
        logger.info(f"failed request: {str(e)}")
        raise AssertionError(f"no weather for '{query}'")

    resp = r.json()

    time = (
        datetime.fromtimestamp(resp["dt"], pytz.UTC)
        .astimezone(place.tz)
        .strftime("%Y-%m-%d %H:%M:%S %Z%z")
    )

    weather = resp["weather"][0]
    status = weather["main"]
    description = weather["description"]
    icon = get_icon(weather["id"], place.time_obj().hour)

    temp = resp["main"]["temp"]
    feels_like = resp["main"]["feels_like"] if "feels_like" in resp["main"] else "—"
    humidity = resp["main"]["humidity"] if "humidity" in resp["main"] else "—"

    wind_speed = resp["wind"]["speed"] if "speed" in resp["wind"] else "—"
    wind_deg = resp["wind"]["deg"] if "deg" in resp["wind"] else "—"
    msg = (
        f"Weather for {place.name}, {place.country.name} ({place.time()}):\n"
        + f"{icon} {status} ({description})\n"
        + f"🌡️  {temp}°C (feels like {feels_like}°C)\n"
        + f"       Humidity {humidity}% Wind {wind_speed}kph {wind_deg}°"
    )
    return msg
