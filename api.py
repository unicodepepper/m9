import logging
import requests
import json

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

token = "TOKEN"
base_url = f"https://api.telegram.org/bot{token}"


def post_message(text, chat_id, parent=None):
    payload = {"chat_id": chat_id, "text": text}

    if parent is not None:
        payload["reply_to_message_id"] = parent

    logger.info("posting payload " + json.dumps(payload))
    return requests.post(f"{base_url}/sendMessage", json=payload)
