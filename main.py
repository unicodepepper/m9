#!/usr/bin/env python3.6
from convert import convert
from fortune import fortune
from annoyance import annoyance
from http.server import HTTPServer, BaseHTTPRequestHandler
import json
import requests
import api
import time_convert
import weather
import duck
import goose
import logging
import consts

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class Handler(BaseHTTPRequestHandler):
    def do_POST(self):
        content_length = int(self.headers["Content-Length"])
        body = self.rfile.read(content_length)
        update = json.loads(body)

        self.send_response(200)
        self.end_headers()

        # some message types don't have text (e.g. new members) so just ignore them
        if "message" not in update or "text" not in update["message"]:
            return

        msg = update["message"]["text"]
        chat_id = update["message"]["chat"]["id"]
        message_id = update["message"]["message_id"]

        tokens = msg.strip().split()
        command = tokens[0]
        if "@" in command:
            command = command[: command.find("@")]
        if command not in consts.COMMANDS:
            # trigger passive message modules here
            passive_msg = passive_message(chat_id, msg)
            if passive_msg is not None:
                api.post_message(passive_msg, chat_id)
            return

        reply = get_message(command, tokens, update)
        api.post_message(reply, chat_id, message_id)


# runs passive message modules, returning a message body if any of them fire
def passive_message(chat_id, incoming_message):
    duck.notify(chat_id)
    goose.notify(chat_id)
    annoyance_msg = annoyance(incoming_message)
    return annoyance_msg


def get_message(command, tokens, update):
    from_id = update["message"]["from"]["id"]
    chat_id = update["message"]["chat"]["id"]
    from_username = update["message"]["from"]["username"]

    try:
        logger.info("reading message " + json.dumps(update["message"]))
        if command == consts.CMD_CONVERT:
            if (
                "reply_to_message" in update["message"]
                and "text" in update["message"]["reply_to_message"]
            ):
                reply_to = update["message"]["reply_to_message"]
                text = reply_to["text"]
                return convert(tokens[1:], text)
            else:
                return convert(tokens[1:])
        elif command == consts.CMD_FORTUNE:
            return fortune(tokens[1:])
        elif command == consts.CMD_TIME:
            return time_convert.time(tokens[1:])
        elif command == consts.CMD_WEATHER:
            return weather.weather(tokens[1:])
        elif command in consts.DUCK_COMMANDS:
            return duck.action(command, from_id, chat_id, from_username)
        elif command in consts.GOOSE_COMMANDS:
            return goose.action(command, from_id, chat_id, from_username)

    except Exception as e:
        logger.exception(e)
        return "error: " + str(e)


def main():
    commands = {
        "commands": [
            {"command": "convert", "description": "convert <number> unit1 (unit2)"},
            {"command": "fortune", "description": "fortune [fortune file]"},
            {"command": "time", "description": "time city"},
            {"command": "weather", "description": "weather city"},
        ]
    }
    r = requests.post(f"{api.base_url}/setMyCommands", json=commands)
    r.raise_for_status()

    logger.debug("setCommands response: " + r.text)

    # set up webhooks
    hook = {"url": "https://qali.net:8443"}
    r = requests.post(f"{api.base_url}/setWebHook", json=hook)
    logger.debug("setWebHook response: " + r.text)
    r.raise_for_status()

    httpd = HTTPServer(("localhost", 8080), Handler)
    httpd.serve_forever()


if __name__ == "__main__":
    main()
