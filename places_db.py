import logging
import pytz
from datetime import datetime

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class Place(object):
    """schema from https://download.geonames.org/export/dump/"""

    def __init__(self, line, country_db):
        super(Place, self).__init__()
        tokens = line.split("\t")

        self.id = tokens[0]
        self.name = tokens[1]
        self.ascii_name = tokens[2]
        self.latitude = tokens[4]
        self.longitude = tokens[5]

        self.country_code = tokens[8]
        assert self.country_code.lower() in country_db
        self.country = country_db[self.country_code.lower()]

        self.population = int(tokens[14])

        assert tokens[17] is not None
        assert len(tokens[17]) > 0
        self.tz = pytz.timezone(tokens[17])

    def time(self):
        now = datetime.now()
        return now.astimezone(self.tz).strftime("%Y-%m-%d %H:%M:%S %Z%z")

    def time_obj(self):
        now = datetime.now()
        return now.astimezone(self.tz)


class Country(object):
    def __init__(self, line):
        super(Country, self).__init__()
        cols = line.split("\t")
        self.iso2 = cols[0]
        self.name = cols[4]


def _load_countries(filename):
    db = {}
    with open(filename) as countries:
        for line in countries:
            try:
                country = Country(line)
                db[country.name.lower()] = country
                db[country.iso2.lower()] = country
            except Exception as e:
                raise

    return db


def _load_places(filename):
    name_db = {}
    db = {}
    with open(filename) as cities:
        for line in cities:
            try:
                place = Place(line, countries_db)
                db[place.id] = place

                # overwrite in name_db if the other place is smaller
                if place.name.lower() in name_db:
                    other = name_db[place.name.lower()]
                    if other.population < place.population:
                        name_db[place.name.lower()] = place
                        name_db[place.ascii_name.lower()] = place
                else:
                    name_db[place.name.lower()] = place
                    name_db[place.ascii_name.lower()] = place

            except Exception as e:
                logger.info("skipping city " + line)

    return (name_db, db)


countries_db = _load_countries("countryInfo.txt")
name_db, db = _load_places("cities500.txt")


def country_search(name, country_or_country_code):
    print(f"searching for {name}, {country_or_country_code}")
    name = name.lower()
    country_or_country_code = country_or_country_code.lower()
    for place in db:
        place = db[place]
        if place.name.lower() == name or place.ascii_name.lower() == name:
            if (
                place.country.name.lower() == country_or_country_code
                or place.country_code.lower() == country_or_country_code
            ):
                return place
    return None


# searches for the given query with the following precedence:
# 1. name, country
# 2. name country
# 3. name
def find_place(query):
    tokens = query.split()
    # override with country name if specified
    if query[-1] != "," and "," in query:
        idx = query.find(",")
        name = query[:idx].strip()
        country = query[idx + 1 :].strip()

        place = country_search(name, country)
        if place != None:
            return place

    # split by space instead if there's only one of them
    if len(tokens) == 2:
        name = tokens[0]
        country = tokens[1]
        place = country_search(name, country)
        if place != None:
            return place

    # fall back to direct search
    if query.lower() in name_db:
        return name_db[query.lower()]

    return None
