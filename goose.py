# -*- coding: utf-8 -*-
import random
import datetime
import json
import logging
import api
from threading import Timer

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def check_goose_times(message_times, geese):
    """
    Loops through all known chats, sending a goose message to the appropriate chat
    if they're supposed to have another goose.
    """
    for chat in message_times.get_stale_chat_ids():
        # don't double-post if no one replied yet
        if geese.has_goose(chat):
            logger.error(f"chat {chat} already has goose, ignoring it")
            continue

        geese.add(chat)

        msg = f"{goose_emoji} HONK! A goose appeared, /stare or /run away from it"
        api.post_message(msg, chat)


class RepeatTimer(Timer):
    # https://stackoverflow.com/a/48741004
    def run(self):
        while not self.finished.wait(self.interval):
            self.function(*self.args, **self.kwargs)


class MessageTimes(object):
    GOOSE_DELAY_SECONDS = 60 * 60 * 5

    """
    Stores the last received message time for chats.
    """

    def __init__(self):
        super(MessageTimes, self).__init__()
        self.times = {}

    def notify(self, chat_id):
        self.times[chat_id] = datetime.datetime.now()

    def get_stale_chat_ids(self):
        """
        Returns a list of chat IDs that are due for a goose.
        """
        return [chat_id for chat_id in self.times if self._should_goose_appear(chat_id)]

    def _should_goose_appear(self, chat_id):
        # bot was just booted, wait for additional messages so we don't spam everyone
        # when I restart it
        if chat_id not in self.times:
            logger.error(f"ignoring quiet chat {chat_id}")
            return False

        dt = datetime.datetime.now() - self.times[chat_id]
        return dt.seconds > self.GOOSE_DELAY_SECONDS


class Geese(object):
    """
    Container for mapping from chat ID -> goose appearances.

    This is used to track which chats have geese visible, and for timing people's responses.
    """

    def __init__(self):
        super(Geese, self).__init__()
        self.geese = {}

    def has_goose(self, chat_id):
        return chat_id in self.geese

    def add(self, chat_id):
        assert not self.has_goose(
            chat_id
        ), "internal error: tried adding a goose to a chat that already has one"
        self.geese[chat_id] = datetime.datetime.now()

    def remove(self, chat_id):
        """
        Removes the goose from the given chat, returning a datetime representing when it was added
        """
        assert self.has_goose(
            chat_id
        ), "internal error: tried removing a goose from a chat that already has one"
        return self.geese.pop(chat_id)


class Stats(object):
    """
    Container for a mapping from user id -> #stares/runs
    """

    filename = "goose_stats.json"

    def __init__(self):
        super(Stats, self).__init__()
        self.stats = {}
        try:
            fp = open(self.filename)
            self.stats = json.load(fp)
        except Exception as e:
            logger.warn("error loading stats, falling back to empty score: " + str(e))

    def _increment(self, user_key, idx):
        scores = self.stats.get(user_key, [0, 0])
        scores[idx] += 1

        self.stats[user_key] = scores
        self._save()

    def increment_stare(self, user_id):
        self._increment(str(user_id), 0)

    def increment_run(self, user_id):
        self._increment(str(user_id), 1)

    def _save(self):
        with open(self.filename, "w") as f:
            json.dump(self.stats, f)

    def get_stare(self, user_id):
        stats = self.stats.get(str(user_id), [0, 0])
        return stats[0]

    def get_run(self, user_id):
        stats = self.stats.get(str(user_id), [0, 0])
        return stats[1]


goose_emoji = "🦢"
stare_emoji = "😒"
run_emoji = "🏃"

TIMEDELTA_SECONDS_MAX = 86399
POLL_FREQ_SECONDS = 60


stats = Stats()
message_times = MessageTimes()
geese = Geese()
timer = RepeatTimer(POLL_FREQ_SECONDS, check_goose_times, args=(message_times, geese))
timer.start()


def notify(chat_id):
    message_times.notify(chat_id)


# returns formatted "in x seconds/days" message
def time_diff(t1, t2):
    logger.info(t1)
    logger.info(t2)
    dt = t2 - t1
    logger.info(dt)

    if dt.seconds == TIMEDELTA_SECONDS_MAX:
        return f"in {dt.days} days"
    elif dt.seconds > 600:
        return f"in {dt.seconds // 60} minutes"
    else:
        return f"in {dt.seconds} seconds"


def action(command, user_id, chat_id, user_name):
    notify(chat_id)  # make sure the command message is counted in message times

    if not geese.has_goose(chat_id):
        if command == "/stare":
            return f"{user_name} stares into space."
        else:
            return (
                f"{user_name} runs away from a drifting feather."
            )

    t1 = geese.remove(chat_id)
    t2 = datetime.datetime.now()
    dt = time_diff(t1, t2)

    if command == "/stare":
        stats.increment_stare(user_id)
    elif command == "/run":
        stats.increment_run(user_id)

    runs = stats.get_run(user_id)
    stares = stats.get_stare(user_id)

    desc = (
        f"{stare_emoji} stared at"
        if command == "/stare"
        else f"{run_emoji} {goose_emoji} ran away"
    )

    return f"{desc} in {dt}.\n{user_name} has run away from {runs} geese and death stared at {stares} geese."
