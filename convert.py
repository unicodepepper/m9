# -*- coding: utf-8 -*-

CENTIMETER = 1
METER = 2
KILOMETER = 3
INCH = 4
FOOT = 5
MILE = 6

CELSIUS = 7
FAHRENHEIT = 8
KELVIN = 13

KILOGRAM = 9
POUND = 10
OUNCE = 11
GRAM = 12

units_display = {
    CENTIMETER: "cm",
    METER: "meters",
    KILOMETER: "kilometers",
    INCH: "inches",
    FOOT: "feet",
    MILE: "miles",
    CELSIUS: "℃",
    FAHRENHEIT: "°F",
    KILOGRAM: "kg",
    POUND: "lb",
    OUNCE: "oz",
    GRAM: "g",
    KELVIN: "K",
}

units = {
    "cm": CENTIMETER,
    "cms": CENTIMETER,
    "centimeters": CENTIMETER,
    "centimetres": CENTIMETER,
    "meters": METER,
    "metres": METER,
    "m": METER,
    "km": KILOMETER,
    "kms": KILOMETER,
    "kilometers": KILOMETER,
    "kilometres": KILOMETER,
    "in": INCH,
    "inch": INCH,
    "inches": INCH,
    "foot": FOOT,
    "ft": FOOT,
    "feet": FOOT,
    '"': INCH,
    "'": FOOT,
    "mi": MILE,
    "miles": MILE,
    "c": CELSIUS,
    "celsius": CELSIUS,
    "f": FAHRENHEIT,
    "kg": KILOGRAM,
    "kgs": KILOGRAM,
    "k": KELVIN,
    "g": GRAM,
    "lb": POUND,
    "lbs": POUND,
    "ounce": OUNCE,
    "ounces": OUNCE,
    "oz": OUNCE,
    "g": GRAM,
}

from decimal import Decimal


def mult(factor):
    return lambda x: x * Decimal(factor)


def div(factor):
    return lambda x: x / Decimal(factor)


conversions = {
    (CENTIMETER, METER): div("100"),
    (CENTIMETER, INCH): mult("0.39370"),
    (METER, CENTIMETER): mult("100"),
    (METER, INCH): mult("39.37008"),
    (INCH, METER): div("39.370"),
    (INCH, CENTIMETER): div("0.39370"),
    (METER, FOOT): mult("3.2808399"),
    (FOOT, METER): div("3.2808"),
    (METER, MILE): mult("0.0006213712"),
    (MILE, METER): div("0.00062137"),
    (KILOMETER, MILE): mult("0.62137"),
    (MILE, KILOMETER): mult("1.609344"),
    (CELSIUS, KELVIN): lambda x: x + Decimal("273.15"),
    (KELVIN, CELSIUS): lambda x: x - Decimal("273.15"),
    (FAHRENHEIT, KELVIN): lambda x: (x + Decimal("459.67")) * Decimal(5) / Decimal(9),
    (KELVIN, FAHRENHEIT): lambda x: ((x * Decimal(9)) / Decimal(5)) - Decimal("459.67"),
    (CELSIUS, FAHRENHEIT): lambda x: (x * Decimal("1.8")) + Decimal(32),
    (FAHRENHEIT, CELSIUS): lambda x: (x - Decimal(32)) / Decimal("1.8"),
    (METER, KILOMETER): div("1000"),
    (KILOMETER, METER): mult("1000"),
    (KILOGRAM, POUND): mult("2.2046"),
    (POUND, KILOGRAM): div("2.2046"),
    (OUNCE, GRAM): div("0.035274"),
    (GRAM, OUNCE): mult("0.035274"),
    (OUNCE, KILOGRAM): mult("0.02834952"),
    (KILOGRAM, OUNCE): div("0.02834952"),
    (OUNCE, POUND): mult("0.062500"),
    (POUND, OUNCE): div("0.062500"),
    (FOOT, INCH): mult("12"),
    (INCH, FOOT): div("12"),
}


class ConversionContext(object):
    """container for qty + source + target units"""

    # TODO: - make to_unit optional and attempt to infer it here
    #       - make these unit values just strings and look up their actual mappings
    #         at conversion time
    def __init__(self, value, from_unit, to_unit):
        super(ConversionContext, self).__init__()
        self.value = value
        self.from_unit = from_unit
        self.to_unit = to_unit

    # conversion helper
    # returns string conversion message
    def convert(self):
        assert (
            self.from_unit in units_display
        ), f"internal error (don't know how to display lhs unit {self.from_unit}"
        assert (
            self.to_unit in units_display
        ), f"internal error (don't know how to display lhs unit {self.to_unit}"
        from_display = units_display[self.from_unit]
        to_display = units_display[self.to_unit]

        assert (
            self.from_unit,
            self.to_unit,
        ) in conversions, (
            f"no conversions available for '{from_display}' to '{to_display}'"
        )

        converted = "%.3f" % conversions[(self.from_unit, self.to_unit)](self.value)
        return f"{self.value} {from_display} in {to_display} = {converted} {to_display}"


# returns message without command
def tokenize(string):
    return string.strip().split()[1:]


# given LHS unit, infer a target unit
def _infer_target_unit(lhs_unit):
    # infer target unit
    if lhs_unit == FOOT:
        return METER
    elif lhs_unit == MILE:
        return KILOMETER
    elif lhs_unit == METER:
        return FOOT
    elif lhs_unit == KILOMETER:
        return MILE
    elif lhs_unit == FAHRENHEIT:
        return CELSIUS
    elif lhs_unit == CELSIUS:
        return FAHRENHEIT
    elif lhs_unit == KILOGRAM:
        return POUND
    elif lhs_unit == POUND:
        return KILOGRAM
    elif lhs_unit == GRAM:
        return OUNCE
    elif lhs_unit == OUNCE:
        return GRAM
    elif lhs_unit == INCH:
        return CENTIMETER
    elif lhs_unit == CENTIMETER:
        return INCH

    return None


def usage():
    return "syntax is '/convert <number> [unit1] [to|from] [unit2]. or reply to a message and I'll grab units and numbers from there"


# splits 1m -> [1, m]
def split_word_to_unit_and_value(word):
    unit_idx = 0
    separator_seen = False
    for i in range(len(word)):
        char = word[i]
        if char.isnumeric():
            i += 1
        elif char in [",", "."]:
            if separator_seen:
                raise AssertionError(
                    f"I can't read '{word}', too many place separators"
                )
            separator_seen = True
            i += 1
        else:
            # found non-numeric non-separator. start reading unit
            unit_idx = i
            break
    else:
        # we couldn't find a unit
        raise AssertionError(f"I can't find a unit in '{word}'")

    assert unit_idx != 0, f"I can't find a number in '{word}'"

    val_str = word[:unit_idx]
    val = _read_num(val_str)

    unit_str = word[unit_idx:]
    unit = _unit_from_str(unit_str)

    return (val, unit)


def _unit_from_str(unit):
    assert unit.lower() in units, f"I don't know what unit '{unit}' is"
    return units[unit.lower()]


def _read_num(string):
    try:
        return Decimal(string.replace(",", "."))
    except Exception as e:
        raise AssertionError(f"can't read '{string}' as a number")


# raw conversion requests
# /convert 1m ft
# /convert 1 m ft
def convert_no_reply(tokens):
    try:
        context = None
        if len(tokens) == 1:
            # /convert 1m
            tok = tokens[0]
            val, from_unit = split_word_to_unit_and_value(tok)
            inferred_to = _infer_target_unit(from_unit)
            assert (
                inferred_to is not None
            ), f"I don't know what to convert '{units_display[from_unit]}' to"

            context = ConversionContext(val, from_unit, inferred_to)
        elif len(tokens) == 2:
            # try reading as /convert 1m inches first
            try:
                val, from_unit = split_word_to_unit_and_value(tokens[0])
                to_unit = _unit_from_str(tokens[1])

                context = ConversionContext(val, from_unit, to_unit)
            except Exception as e:
                # fall back to /convert 1 m
                try:
                    val = _read_num(tokens[0])
                    from_unit = _unit_from_str(tokens[1])
                    to_unit = _infer_target_unit(from_unit)
                    context = ConversionContext(val, from_unit, to_unit)
                except Exception as e:
                    raise AssertionError(f"can't parse: {str(e)}")

        elif len(tokens) == 3:
            if tokens[1].lower() in ["to", "in"]:
                # /convert 1m {to|in} inches
                val, from_unit = split_word_to_unit_and_value(tokens[0])
                to_unit = _unit_from_str(tokens[2])
                context = ConversionContext(val, from_unit, to_unit)
            else:
                # /convert 1 m inches
                val = _read_num(tokens[0])
                from_unit = _unit_from_str(tokens[1])
                to_unit = _unit_from_str(tokens[2])
                context = ConversionContext(val, from_unit, to_unit)
        elif len(tokens) == 4:
            # /convert 1 m {to|in} inches
            assert tokens[2].lower() in ["to", "in"], "I don't know what you're asking"
            val = _read_num(tokens[0])
            from_unit = _unit_from_str(tokens[1])
            to_unit = _unit_from_str(tokens[3])
            context = ConversionContext(val, from_unit, to_unit)
        else:
            raise AssertionError("couldn't understand you")

        assert context is not None, f"internal error (conversion context is unset)"
        return context.convert()
    except Exception as e:
        raise AssertionError(f"{str(e)}. {usage()}")


# for conversion requests in reply to a message
def convert_reply(tokens, reply_to):
    reply_tokens = reply_to.strip().split()

    # picks first number
    def find_number(tokens):
        import re

        number_regex = re.compile(r"^\d+[.,]?\d*$")

        for word in tokens:
            if number_regex.match(word):
                return _read_num(word)

        return None

    # attempt to extract a number + unit from the given text
    def find_unit_usage(tokens):
        # first, we look for number and unit without spaces (300m)
        for word in tokens:
            try:
                return split_word_to_unit_and_value(word)
            except Exception as e:
                pass

        # then we fall back to looking at pairs of adjacent words (300 m) etc.
        for num, unit in zip(tokens, tokens[1:]):
            try:
                num = _read_num(num)
                unit = _unit_from_str(unit)
                return (num, unit)
            except Exception as e:
                pass

        raise AssertionError("I couldn't find a number in the linked message")

    if len(tokens) == 0:
        # /convert (implicit)
        num, from_unit = find_unit_usage(reply_tokens)
        to_unit = _infer_target_unit(from_unit)
        return ConversionContext(num, from_unit, to_unit).convert()
    elif len(tokens) == 1:
        # /convert m
        to_unit = _unit_from_str(tokens[0])
        num, from_unit = find_unit_usage(reply_tokens)
        return ConversionContext(num, from_unit, to_unit).convert()
    elif len(tokens) == 2:
        # /convert m km
        num = find_number(reply_tokens)
        if num is None:
            raise AssertionError("I couldn't find a number in the linked message")
        from_unit = _unit_from_str(tokens[0])
        to_unit = _unit_from_str(tokens[1])
        return ConversionContext(num, from_unit, to_unit).convert()
    else:
        raise AssertionError(usage())


def convert(tokens, reply_to=None):
    if reply_to is None:
        return convert_no_reply(tokens)
    else:
        return convert_reply(tokens, reply_to)
