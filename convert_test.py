import unittest
from convert import convert_no_reply, convert_reply


class ConvertTest(unittest.TestCase):
    def test_no_reply(self):
        cases = [
            ["explicit", "10 oz g", "10 oz in g = 283.495 g"],
            ["implicit space", "10 oz", "10 oz in g = 283.495 g"],
            ["implicit single character", "30c", "30 ℃ in °F = 86.000 °F"],
            ["explicit no space", "10oz g", "10 oz in g = 283.495 g"],
            ["implicit no space", "10oz", "10 oz in g = 283.495 g"],
            ["to no space", "1m to feet", "1 meters in feet = 3.281 feet"],
            ["in space", "1 m in feet", "1 meters in feet = 3.281 feet"],
        ]

        for case in cases:
            with self.subTest(msg=case[0]):
                tokens = case[1].split()
                self.assertEqual(case[2], convert_no_reply(tokens))

    def test_reply(self):
        cases = [
            [
                "explicit",
                "km m",
                "it was around 10 away",
                "10 kilometers in meters = 10000.000 meters",
            ],
            [
                "target explicit no spaces",
                "m",
                "it was around 10km away",
                "10 kilometers in meters = 10000.000 meters",
            ],
            [
                "target explicit spaces",
                "m",
                "it was around 10 km away",
                "10 kilometers in meters = 10000.000 meters",
            ],
            [
                "implicit spaces",
                "",
                "it was around 10 km away",
                "10 kilometers in miles = 6.214 miles",
            ],
            [
                "implicit no spaces",
                "",
                "it was around 10km away",
                "10 kilometers in miles = 6.214 miles",
            ],
        ]

        for case in cases:
            with self.subTest(msg=case[0]):
                tokens = case[1].split()
                reply_text = case[2]

                self.assertEqual(case[3], convert_reply(tokens, reply_text))


if __name__ == "__main__":
    unittest.main()
