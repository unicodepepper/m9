# -*- coding: utf-8 -*-
import random
import datetime
import json
import logging
import api
from threading import Timer

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def check_duck_times(message_times, ducks):
    """
    Loops through all known chats, sending a duck message to the appropriate chat
    if they're supposed to have another duck.
    """
    for chat in message_times.get_stale_chat_ids():
        # don't double-post if no one replied yet
        if ducks.has_duck(chat):
            logger.error(f"chat {chat} already has duck, ignoring it")
            continue

        ducks.add(chat)

        msg = f"{duck_emoji} QUACK! A duck appeared, /shoot or /friend it"
        api.post_message(msg, chat)


class RepeatTimer(Timer):
    # https://stackoverflow.com/a/48741004
    def run(self):
        while not self.finished.wait(self.interval):
            self.function(*self.args, **self.kwargs)


class MessageTimes(object):
    DUCK_DELAY_SECONDS = 60 * 60 * 6

    """
    Stores the last received message time for chats.
    """

    def __init__(self):
        super(MessageTimes, self).__init__()
        self.times = {}

    def notify(self, chat_id):
        self.times[chat_id] = datetime.datetime.now()

    def get_stale_chat_ids(self):
        """
        Returns a list of chat IDs that are due for a duck.
        """
        return [chat_id for chat_id in self.times if self._should_duck_appear(chat_id)]

    def _should_duck_appear(self, chat_id):
        # bot was just booted, wait for additional messages so we don't spam everyone
        # when I restart it
        if chat_id not in self.times:
            logger.error(f"ignoring quiet chat {chat_id}")
            return False

        dt = datetime.datetime.now() - self.times[chat_id]
        return dt.seconds > self.DUCK_DELAY_SECONDS


class Ducks(object):
    """
    Container for mapping from chat ID -> duck appearances.

    This is used to track which chats have ducks visible, and for timing people's responses.
    """

    def __init__(self):
        super(Ducks, self).__init__()
        self.ducks = {}

    def has_duck(self, chat_id):
        return chat_id in self.ducks

    def add(self, chat_id):
        assert not self.has_duck(
            chat_id
        ), "internal error: tried adding a duck to a chat that already has one"
        self.ducks[chat_id] = datetime.datetime.now()

    def remove(self, chat_id):
        """
        Removes the duck from the given chat, returning a datetime representing when it was added
        """
        assert self.has_duck(
            chat_id
        ), "internal error: tried removing a duck from a chat that already has one"
        return self.ducks.pop(chat_id)


class Stats(object):
    """
    Container for a mapping from user id -> #shots/saves
    """

    filename = "duck_stats.json"

    def __init__(self):
        super(Stats, self).__init__()
        self.stats = {}
        try:
            fp = open(self.filename)
            self.stats = json.load(fp)
        except Exception as e:
            logger.warn("error loading stats, falling back to empty score: " + str(e))

    def _increment(self, user_key, idx):
        scores = self.stats.get(user_key, [0, 0])
        scores[idx] += 1

        self.stats[user_key] = scores
        self._save()

    def increment_shoot(self, user_id):
        self._increment(str(user_id), 0)

    def increment_friend(self, user_id):
        self._increment(str(user_id), 1)

    def _save(self):
        with open(self.filename, "w") as f:
            json.dump(self.stats, f)

    def get_shoot(self, user_id):
        stats = self.stats.get(str(user_id), [0, 0])
        return stats[0]

    def get_friend(self, user_id):
        stats = self.stats.get(str(user_id), [0, 0])
        return stats[1]


duck_emoji = "🦆"
bang_emoji = "💥"
heart_emoji = "🥰"

TIMEDELTA_SECONDS_MAX = 86399
POLL_FREQ_SECONDS = 60


stats = Stats()
message_times = MessageTimes()
ducks = Ducks()
timer = RepeatTimer(POLL_FREQ_SECONDS, check_duck_times, args=(message_times, ducks))
timer.start()


def notify(chat_id):
    message_times.notify(chat_id)


# returns formatted "in x seconds/days" message
def time_diff(t1, t2):
    logger.info(t1)
    logger.info(t2)
    dt = t2 - t1
    logger.info(dt)

    if dt.seconds == TIMEDELTA_SECONDS_MAX:
        return f"in {dt.days} days"
    elif dt.seconds > 600:
        return f"in {dt.seconds // 60} minutes"
    else:
        return f"in {dt.seconds} seconds"


def action(command, user_id, chat_id, user_name):
    notify(chat_id)  # make sure the command message is counted in message times

    if not ducks.has_duck(chat_id):
        if command == "/shoot":
            return f"{user_name} grabs their gun and aims at nothing in particular."
        else:
            return (
                f"{user_name} throws some breadcrumbs on the ground. nothing happens."
            )

    t1 = ducks.remove(chat_id)
    t2 = datetime.datetime.now()
    dt = time_diff(t1, t2)

    if command == "/shoot":
        stats.increment_shoot(user_id)
    elif command == "/friend":
        stats.increment_friend(user_id)

    friends = stats.get_friend(user_id)
    shots = stats.get_shoot(user_id)

    desc = (
        f"{bang_emoji} shot"
        if command == "/shoot"
        else f"{heart_emoji} {duck_emoji} befriended"
    )

    return f"{desc} in {dt}.\n{user_name} has befriended {friends} ducks and shot {shots} ducks."
