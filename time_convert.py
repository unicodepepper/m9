import places_db


def time(tokens):
    assert len(tokens) > 0, "usage: /time city"
    query = " ".join(tokens)
    place = places_db.find_place(query)
    assert place is not None, f"Couldn't find {query}"

    country = place.country
    return f"Time in {place.name}, {country.name}: {place.time()}"
